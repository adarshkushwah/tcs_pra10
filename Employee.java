package com;

public class Employee {
private int id;
private String name;
private String designation;
private int  SsNo;
private int age;
public Employee(int id, String name, String designation, int ssNo, int age) {
	super();
	this.id = id;
	this.name = name;
	this.designation = designation;
	this.SsNo = ssNo;
	this.age = age;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getDesignation() {
	return designation;
}
public void setDesignation(String designation) {
	this.designation = designation;
}
public int getSsNo() {
	return SsNo;
}
public void setSsNo(int ssNo) {
	this.SsNo = ssNo;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}


	
	
	
}
